The website is hosted on an ec2 instance (http://3.25.201.132:4200/) with inbound traffic able to access to port 4200.
It is difficult for ToneJS to use mp3 files locally so all the drum sounds are hosted on a heroku server. This means you also need to install a [CORS chrome extension](https://chrome.google.com/webstore/detail/moesif-origin-cors-change/digfbfaphojjndkpccljibejjbppifbc?hl=en) to use the drum grid.

Started with some unit tests but lost interest so they are incomplete
