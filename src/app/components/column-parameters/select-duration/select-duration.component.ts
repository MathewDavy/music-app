import { Component, Input, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { ColumnParameter } from '../ColumnParameter';
import { DurationService } from './duration.service'
import { Grid } from '../../grids/Grid';






@Component({
  selector: 'app-select-duration',
  templateUrl: './select-duration.component.html',
  styleUrls: ['./select-duration.component.scss']
})
export class SelectDurationComponent extends ColumnParameter {

  public durationArray: string[];
  @Input() gridService: Grid;


  constructor(public durationService: DurationService) { 
    super();
    this.durationArray = ['wholeNote', 'halfNote', 'quarterNote', 'eighthNote']
  }

}
