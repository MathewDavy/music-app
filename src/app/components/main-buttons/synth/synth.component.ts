import { Component, OnInit,Input } from '@angular/core';
import { SynthService } from './synth-service'
import { Grid } from '../../grids/Grid';



@Component({
  selector: 'app-synth',
  templateUrl: './synth.component.html',
  styleUrls: ['./synth.component.scss']
})
export class SynthComponent {

  @Input() gridService: Grid;

  constructor(public synthService: SynthService) {

  }
  
  setSynth = (synth: string) => {

    
    this.gridService.synthName = synth
    this.gridService.setSynth(this.gridService.polySynth.volume.value)

  }

}
