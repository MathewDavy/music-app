import { Component, OnInit } from '@angular/core';
import * as Tone from "tone";

@Component({
  selector: 'app-bpm',
  templateUrl: './bpm.component.html',
  styleUrls: ['./bpm.component.scss']
})
export class BpmComponent implements OnInit {

  public slider: number;
  public min: number;
  public max: number;

  constructor() { 
    this.min = 50;
    this.max = 300;
    this.slider = ((this.max - this.min) / 2) + this.min
  }

  ngOnInit(): void {
  }

  onChange(value: number): number {
    Tone.Transport.bpm.value = value;
    return value
  }

}
