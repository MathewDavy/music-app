import { Component, OnInit, AfterViewInit } from '@angular/core';
import { DrumGridService } from './drum-grid.service';

@Component({
  selector: 'app-drum-grid',
  templateUrl: './drum-grid.component.html',
  styleUrls: ['./drum-grid.component.scss']
})
export class DrumGridComponent implements AfterViewInit {

  constructor(public drumGridService: DrumGridService) {
 
  }

  ngAfterViewInit():void{
  }

}
