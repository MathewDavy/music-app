import { Component, AfterViewInit } from '@angular/core';
import { MelodyGridService } from './melody-grid.service';



@Component({
  selector: 'app-melody-grid',
  templateUrl: './melody-grid.component.html',
  styleUrls: ['./melody-grid.component.scss']
})
export class MelodyGridComponent implements AfterViewInit{

  constructor(public melodyGridService: MelodyGridService) {
 
  }

  ngAfterViewInit():void{
    //want the melody grid to have 7 columns but still need to set 8 in constructor
   this.melodyGridService.durationBtns.pop()
   this.melodyGridService.numColsArr.pop()
  }



}
