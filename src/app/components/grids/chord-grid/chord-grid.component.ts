import { Component, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { DurationService } from '../../column-parameters/select-duration/duration.service';
import { MelodyGridService } from '../melody-grid/melody-grid.service';
import { ChordGridService } from './chord-grid.service';

@Component({
  selector: 'app-chord-grid',
  templateUrl: './chord-grid.component.html',
  styleUrls: ['./chord-grid.component.scss']
})
export class ChordGridComponent implements OnInit, AfterViewInit {

  constructor(public durationService: DurationService, public melodyGridService: MelodyGridService, public chordGridService: ChordGridService, private cdref: ChangeDetectorRef) {
 
  }
  ngOnInit(): void {
  
  }

  ngAfterViewInit(){
    
    let durations: string[] = [];
    for (let i = 0; i <= this.chordGridService.numColsArr.length; i++){
      durations.push(this.chordGridService.startingDuration)
    }
    this.durationService.setupMapWidths(this.melodyGridService.numColsArr.length)
    this.durationService.setDurations(durations,this.chordGridService)
    this.cdref.detectChanges();

  }

}
