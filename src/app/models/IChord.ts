
export interface IChord {
  
  notes: string[],
  name?: string,
}