export enum Icons {
    sine = 'sine',
    triangle = 'triangle',
    square = 'square',
    sawtooth = 'sawtooth',

    add = 'add',
    chord = 'chord',
    melody = 'melody',
    play = 'play',
    stop = 'stop',
    subtract = 'subtract',
    volume = 'volume',

    eighthNote = 'eighthNote',
    halfNote = 'halfNote',
    quarterNote = 'quarterNote',
    wholeNote = 'wholeNote',

    hihat = 'hihat',
    kick = 'kick',
    snare = 'snare',


    


}